# `pgsc_calc` Test Dataset

To test the pipeline we provide a small extract of the synthetic genotype data from the Common Infrastructure for
National Cohorts in Europe, Canada, and Africa ([CINECA](https://www.cineca-project.eu)) project. For more information
about the synthetic data see: <https://www.cineca-project.eu/cineca-synthetic-datasets>. The data were obtained from
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.5082689.svg)](https://doi.org/10.5281/zenodo.5082689) under a
[Creative Commons Attribution Non-Commercial Share-Alike license](https://creativecommons.org/licenses/by-nc-sa/4.0/).
